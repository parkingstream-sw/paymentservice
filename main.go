package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	log "github.com/sirupsen/logrus"
)

var db *gorm.DB

type Users struct {
	IdUser         int `gorm:"primary_key"`
	Name           string
	LastName       string
	Home_Address   string
	Billing_Adress string
	BillingAddress string

	Wallet_Amount float32
	CCNum         string
	CCExp         time.Time
	CCVC          string
}
type Plates struct {
	IdPlate     int `gorm:"primary_key"`
	User        Users
	PlateNumber string
}

type streams_billing struct {
	IdStream    int `gorm:"primary_key"`
	StartArea   time.Time
	EndArea     time.Time
	PlateNumber string
	IdCamera    string
	StartPark   time.Time
	EndPark     time.Time
}

type streams_history struct {
	Id    int `gorm:"primary_key"`
	Date  time.Time
	Plate string
	CamId string
}
type Config struct {
	Database struct {
		Host        string `yaml:"host" env:"DB_HOST" env-description:"Database host"`
		Port        string `yaml:"port" env:"DB_PORT" env-description:"Database port"`
		Username    string `yaml:"username" env:"DB_USER" env-description:"Database user name"`
		Password    string `yaml:"password" env:"DB_PASSWORD" env-description:"Database user password"`
		Name        string `yaml:"db-name" env:"DB_NAME" env-description:"Database name"`
		Connections int    `yaml:"connections" env:"DB_CONNECTIONS" env-description:"Total number of database connections"`
	} `yaml:"database"`
	Server struct {
		Host string `yaml:"host" env:"SRV_HOST,HOST" env-description:"Server host" env-default:"localhost"`
		Port string `yaml:"port" env:"SRV_PORT,PORT" env-description:"Server port" env-default:"8080"`
	} `yaml:"server"`
	Greeting string `env:"GREETING" env-description:"Greeting phrase" env-default:"Hello!"`
}

// Args command-line parameters
type Args struct {
	ConfigPath string
}

func Healthz(w http.ResponseWriter, r *http.Request) {
	log.Info("API Health is OK")
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, `{"alive": true}`)
}

func init() {
	log.SetFormatter(&log.TextFormatter{})
	log.SetReportCaller(true)
}

func main() {
	defer db.Close()

	var cfg Config

	args := ProcessArgs(&cfg)

	// read configuration from the file and environment variables
	if err := cleanenv.ReadConfig(args.ConfigPath, &cfg); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	fmt.Printf("%s:%s@tcp(%s)/parkingstream?charset=utf8&parseTime=True&loc=Local", cfg.Database.Username, cfg.Database.Password, cfg.Database.Host)
	db, _ = gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local", cfg.Database.Username, cfg.Database.Password, cfg.Database.Host, cfg.Database.Name))

	db.Debug().AutoMigrate(&Users{})
	db.Debug().AutoMigrate(&Plates{})
	db.Debug().AutoMigrate(&streams_billing{})
	db.Debug().AutoMigrate(&streams_history{})

	log.Info("Starting Payment API server")
	router := mux.NewRouter()
	router.HandleFunc("/healthz", Healthz).Methods("GET")
	router.HandleFunc("/streamapi", StreamApi).Methods("GET")
	router.HandleFunc("/topup", TopUp).Methods("POST")
	http.ListenAndServe(":3000", router)
}

func TopUp(w http.ResponseWriter, r *http.Request) {
	amount := r.FormValue("amount")
	log.WithFields(log.Fields{"amount": amount}).Info("Toping Up the existing user")

	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, `{"message": true}`)

}

func StreamApi(w http.ResponseWriter, r *http.Request) {

	query := r.URL.Query()
	plateno := query.Get("plateno")
	camid := query.Get("camid")
	streamhistory := streams_history{CamId: camid, Plate: plateno, Date: time.Now()}
	db.Debug().Create(&streamhistory)
	db.Debug().Save(&streamhistory)
	fmt.Print(camid)
	var stream streams_billing
	var result = db.Where(&streams_billing{PlateNumber: plateno}).First(&stream)
	fmt.Print(&stream)
	if result.RowsAffected == 0 {
		var streamtocheck = streams_billing{
			StartArea:   time.Now(),
			EndArea:     time.Now(),
			PlateNumber: plateno,
			IdCamera:    camid,
			StartPark:   time.Now(),
			EndPark:     time.Now(),
		}
		db.Debug().Create(&streamtocheck)
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"billing": false}`)
	} else {
		stream.IdCamera = camid

		var end = stream.StartPark.Add(20 * time.Minute)
		if time.Now().Before(end) {

			stream.StartPark = time.Now()
			stream.EndPark = stream.StartPark
			stream.EndArea = stream.StartPark
			w.Header().Set("Content-Type", "application/json")
			io.WriteString(w, `{"billing": false}`)

		} else {
			stream.EndArea = time.Now()
			stream.EndPark = stream.EndArea
			w.Header().Set("Content-Type", "application/json")
			io.WriteString(w, `{"billing received": true}`)
		}
		db.Debug().Save(&stream)

	}

}
func ProcessArgs(cfg interface{}) Args {
	var a Args

	f := flag.NewFlagSet("Example server", 1)
	f.StringVar(&a.ConfigPath, "c", "config.yml", "Path to configuration file")

	fu := f.Usage
	f.Usage = func() {
		fu()
		envHelp, _ := cleanenv.GetDescription(cfg, nil)
		fmt.Fprintln(f.Output())
		fmt.Fprintln(f.Output(), envHelp)
	}

	f.Parse(os.Args[1:])
	return a
}
