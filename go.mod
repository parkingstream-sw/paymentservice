module bitbucket.org/CharisParkMan

go 1.13

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/jinzhu/gorm v1.9.16
	github.com/sirupsen/logrus v1.7.0
)
